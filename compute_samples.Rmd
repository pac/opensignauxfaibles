---
title: "Compute whole samples"
output:
  html_document: default
  html_notebook: default
params: 
  start: "2013-01-01"
  end: "2017-07-01"
  last: "2017-05-01"
---

```{r setup}
library("opensignauxfaibles")
library("dplyr")
database_signauxfaibles <- database_connect()
```

```{r list-tables}
src_tbls(x = database_signauxfaibles)
```

## Effectif

```{r compute-effectif}
compute_wholesample_effectif(
  db = database_signauxfaibles,
  name = "wholesample_effectif",
  start = params$start,
  end = params$end, 
  last = params$last)
```

## Altares

```{r}
compute_wholesample_altares(
  db = database_signauxfaibles,
  name = "wholesample_altares",
  start = params$start,
  end = params$end)
```

### Préfiltre ALTARES

```{r}
compute_wholesample_prefilter_altares(
  db = database_signauxfaibles,
  name = "wholesample_prefilter_altares",
  start = params$start,
  end = params$end
  )
```

## Activité partielle

```{r}
compute_wholesample_apart(
  db = database_signauxfaibles,
  name = "wholesample_apart",
  start = params$start,
  end = params$end
  )
```

```{r}
compute_wholesample_apartconsommee(
  db = database_signauxfaibles,
  name = "wholesample_apartconsommee",
  start = params$start,
  end = params$end
  )
```

## Cotisations moyennes

```{r}
compute_wholesample_meancotisation(
  db = database_signauxfaibles,
  name = "wholesample_meancotisation",
  start = params$start,
  end = params$end
  )
```

## Dette cumulée 

```{r}
compute_wholesample_dettecumulee(
  db = database_signauxfaibles,
  name = "wholesample_dettecumulee",
  start = params$start,
  end = params$end
  )
```

```{r}
compute_wholesample_lagdettecumulee(
  db = database_signauxfaibles,
  name = "wholesample_lagdettecumulee",
  start = params$start,
  end = params$end
  )
```

## Nombre de débits

```{r}
compute_wholesample_nbdebits(
  db = database_signauxfaibles,
  name = "wholesample_nbdebits",
  start =  params$start,
  end = params$end)
```

## Délais URSSAF

```{r}
compute_wholesample_delais(
  db = database_signauxfaibles,
  name = "wholesample_delais",
  start = params$start,
  end = params$end
  )
```

## Plan CCSV

```{r}
compute_wholesample_ccsv(
  db = database_signauxfaibles,
  name = "wholesample_ccsv",
  start = params$start,
  end = params$end
  )
```

## Whole sample

```{r}
compute_wholesample(
  db = database_signauxfaibles, 
  name = "wholesample"
  )
```

```{r}
wholesample <- collect_wholesample(db = database_signauxfaibles, table = "wholesample")
```

```{r}
readr::write_csv(wholesample, path = "data/wholesample.csv")
save(wholesample, file = "data/wholesample.Rda")
```

